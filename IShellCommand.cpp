#include <Windows.h>
#include <iostream>
#include <vector>
#include "Helper.h"

using namespace std;

string PWD(bool print)
{
	char buffer[MAX_PATH+1];
	if (!GetCurrentDirectoryA(sizeof(buffer), buffer)) 
		cerr << "error";
	if (print) cout << buffer << endl;
	return buffer;
}

void cd(string path)
{
	char* pathArray = new char[path.size() + 1];
	pathArray[path.size()] = 0;
	memcpy(pathArray, path.c_str(), path.size());

	if (!SetCurrentDirectory(pathArray))
		cerr << "error" << endl;
}

void create(string name)
{
	char * fileName = new char[name.size() + 1];
	fileName[name.size()] = 0;
	memcpy(fileName, name.c_str(), name.size());
	if (!CreateFile(fileName, GENERIC_WRITE, 0, NULL, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, NULL))
		cerr << "error" << endl;
	
}

void ls(string path)
{
	HANDLE hFind;
	WIN32_FIND_DATA data;

	path += "\\*.*";
	if (!(hFind=FindFirstFile(path.c_str(), &data)))
	{
		cerr << "error" << endl;
		return;
	}
	hFind = FindFirstFile(path.c_str(), &data);
	if (hFind != INVALID_HANDLE_VALUE)
		do {
			printf("%s\n", data.cFileName);
		} while (FindNextFile(hFind, &data));
}

void execute(string commandLine)
{

   PROCESS_INFORMATION procHandles;
    STARTUPINFOA startWinInfo;
    BOOL    result;

	char * file = (char*)commandLine.c_str();

	DWORD  flags, waitStatus = 0, procStatus = 0, inloop = 1;

    memset(&startWinInfo, 0, sizeof(startWinInfo));
    startWinInfo.cb = sizeof(startWinInfo);

    flags = (CREATE_DEFAULT_ERROR_MODE);

    procHandles.hProcess = INVALID_HANDLE_VALUE;
    procHandles.hThread  = INVALID_HANDLE_VALUE;

    result = CreateProcessA(
        file, NULL, NULL, NULL, 0, flags, NULL,
        NULL, &startWinInfo, &procHandles);

    if (result == 0)
    {
		cerr << "error" << endl;
		return;
    }


    waitStatus = WaitForInputIdle(procHandles.hProcess, 6000);
    inloop = GetExitCodeProcess(procHandles.hProcess, &procStatus);

    while (procStatus == STILL_ACTIVE && inloop)
    {
        waitStatus = WaitForSingleObject(procHandles.hProcess, 30000);
        if (waitStatus == WAIT_TIMEOUT)
        {
            inloop = 1;    
        }
        else if (waitStatus == WAIT_OBJECT_0)
        {
            inloop = GetExitCodeProcess(procHandles.hProcess, &procStatus);
        }
        else
        {
			cerr << "error" << endl;
			return;
        }
    }
    CloseHandle(procHandles.hProcess);
    CloseHandle(procHandles.hThread);
}

int main()
{
	vector<string> commandLine;
	string line;

	while (true)
	{
		cout << ">>>";

		getline(cin, line);
		Helper::trim(line);
		commandLine = Helper::get_words(line);

		if (commandLine[0] == "pwd")
		{
			PWD(true);
			cout << endl;
		}
		else if (commandLine[0] == "cd")
		{
			if (commandLine.size() > 1)
			{
				line = PWD(false);
				line += '\\';
				line += commandLine[1];
				cd(line);
			}
			else
				cerr << "error" << endl;
		}
		else if (commandLine[0] == "create")
		{
			if (commandLine.size() > 1)
			{
				create(commandLine[1]);
			}
			else
				cerr << "error" << endl;
		}
		else if (commandLine[0] == "ls")
		{
			ls(PWD(false));
		}
		else if (commandLine[0].find(".exe") != string::npos)
		{
			execute(commandLine[0]);
		}
	}
}

